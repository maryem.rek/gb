import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bug } from '../../models/bug';

@Injectable({
  providedIn: 'root'
})
export class BugService {

  private baseUrl = 'http://localhost:8080/api/Bug';

  constructor(private http:HttpClient) { }

  getAllBugs(){
    return this.http.get(this.baseUrl)
  }
  getOneBug(id:number){
    // return this.http.get('http://localhost:8081/api/bug/'+id)
    return this.http.get(`${this.baseUrl}/${id}`)
  }

  addBug(bug:Bug){
    return this.http.post(this.baseUrl,bug)
    
  }
  ubdateBugs(bug:Bug){
    return this.http.put(this.baseUrl,bug)
  }
  deleteBugs(id:number){
    return this.http.delete(`${this.baseUrl}/${id}`,{responseType:'text'})
  }
}
