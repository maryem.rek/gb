import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Description } from '../../models/description';
@Injectable({
  providedIn: 'root'
})
export class DescriptionServiceService {

  constructor(private http:HttpClient){}
  getAllDescription(){
    return this.http.get('http://localhost:8081/api/description')
  }
  getOneDescription(id:number){
    return this.http.get(`http://localhost:8081/api/description/${id}`)
  }

  addDescription(description:Description){
    return this.http.post('http://localhost:8081/api/description',description)
    
  }
  updateDescription(description:Description){
    return this.http.put('http://localhost:8081/api/description',description)
  }
  deleteDescription(id:number){
    
    return this.http.delete(`http://localhost:8081/api/description/${id}`, { responseType: 'text' });
  }
}
