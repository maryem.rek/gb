import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})

export class UserServiceService {

  constructor(private http:HttpClient) { }
  getAllUser(){
    return this.http.get('http://localhost:8081/api/user')
  }
  getOneBug(id:number){
    // return this.http.get('http://localhost:8081/api/bug/'+id)
    return this.http.get(`http://localhost:8081/api/user/${id}`)
  }

  addBug(user:User){
    return this.http.post('http://localhost:8081/api/user',user)
    
  }
  ubdateBugs(user:User){
    return this.http.put('http://localhost:8081/api/user',user)
  }
  deleteBugs(id:number){
    return this.http.delete(`http://localhost:8081/api/user/${id}`,{responseType:'text'})
  }
}
